# -*- coding: utf-8 -*-

def chiffrement(texte, decalage):
    """
    Cette fonction prend en entrée un texte et un décalage entier,
    puis retourne le texte chiffré avec le chiffrement de César.
    """
    texte_chiffre = ""
    for lettre in texte:
        # Si la lettre est une lettre majuscule
        if lettre.isupper():
            # On applique le décalage et on s'assure que le résultat reste une lettre majuscule
            texte_chiffre += chr((ord(lettre) + decalage - 65) % 26 + 65)
        # Si la lettre est une lettre minuscule
        elif lettre.islower():
            # On applique le décalage et on s'assure que le résultat reste une lettre minuscule
            texte_chiffre += chr((ord(lettre) + decalage - 97) % 26 + 97)
        else:
            # Si la lettre n'est pas une lettre alphabétique, on la laisse telle quelle
            texte_chiffre += lettre
    return texte_chiffre

def dechiffrement(texte_chiffre, decalage):
    """
    Cette fonction prend en entrée un texte chiffré avec le chiffrement de César et un décalage entier,
    puis retourne le texte déchiffré.
    """
    texte_dechiffre = ""
    for lettre in texte_chiffre:
        # Si la lettre est une lettre majuscule
        if lettre.isupper():
            # On applique l'inverse du décalage et on s'assure que le résultat reste une lettre majuscule
            texte_dechiffre += chr((ord(lettre) - decalage - 65) % 26 + 65)
        # Si la lettre est une lettre minuscule
        elif lettre.islower():
            # On applique l'inverse du décalage et on s'assure que le résultat reste une lettre minuscule
            texte_dechiffre += chr((ord(lettre) - decalage - 97) % 26 + 97)
        else:
            # Si la lettre n'est pas une lettre alphabétique, on la laisse telle quelle
            texte_dechiffre += lettre
    return texte_dechiffre

if __name__ == "__main__":
    texte_original = input("Entrez le message a chiffrer: ")
    decalage = int(input("Entrez la valeur du decalage: "))

    texte_chiffre = chiffrement(texte_original, decalage)
    print("Texte chiffre :", texte_chiffre)

    texte_dechiffre = dechiffrement(texte_chiffre, decalage)
    print("Texte dechiffre :", texte_dechiffre) 