import random
from math import pow
from PIL import Image
import io, os
 
a = random.randint(2, 10)
 
def pgcd(a, b):
    if a < b:
        return pgcd(b, a)
    elif a % b == 0:
        return b;
    else:
        return pgcd(b, a % b)
 
# Generating large random numbers
def gen_key(q):
    key = random.randint(pow(10, 20), q)
    while pgcd(q, key) != 1:
        key = random.randint(pow(10, 20), q)
 
    return key
 
# Modular exponentiation
def power(a, b, c):
    x = 1
    y = a
 
    while b > 0:
        if b % 2 != 0:
            x = (x * y) % c;
        y = (y * y) % c
        b = int(b / 2)
 
    return x % c
 
# Asymmetric encryption
def encrypt(msg, q, h, g):
 
    en_msg = []
 
    k = gen_key(q)# Private key for sender
    s = power(h, k, q)
    p = power(g, k, q)
     
    for i in range(0, len(msg)):
        en_msg.append(msg[i])
 
    print("g^k used : ", p)
    print("g^ak used : ", s)
    for i in range(0, len(en_msg)):
        en_msg[i] = s * ord(en_msg[i])
 
    return en_msg, p
 
def decrypt(en_msg, p, key, q):
 
    dr_msg = []
    h = power(p, key, q)
    for i in range(0, len(en_msg)):
        dr_msg.append(chr(int(en_msg[i]/h)))
         
    return dr_msg

def convert_img(filename):
    with Image.open(filename) as img:
        # Conversion de l'image en données binaires
        img_bytes = io.BytesIO()
        img.save(img_bytes, format=img.format)
        img_bytes = img_bytes.getvalue()
    return img_bytes
 
# Driver code
def main_text():
 
    msg = 'encryption'
    print("Original Message :", msg)
 
    q = random.randint(pow(10, 20), pow(10, 50))
    g = random.randint(2, q)
 
    private_key = gen_key(q)# Private key for receiver
    public_key = power(g, private_key, q)# Public key for receiver
    print("g used : ", g)
    print("g^a used public key : ", public_key)
 
    en_msg, p = encrypt(msg, q, public_key, g)
    dr_msg = decrypt(en_msg, p, private_key, q)
    dmsg = ''.join(dr_msg)
    print("Decrypted Message :", dmsg);
    
def main_image():

    # Chargement de l'image
    img_filename = "myimage.jpg"
    img_data = convert_img(img_filename)
    print("Original Image Size:", len(img_data))

    # Génération des clés
    q = random.randint(pow(10, 20), pow(10, 50))
    g = random.randint(2, q)
    private_key = gen_key(q)
    public_key = power(g, private_key, q)
    print("g used : ", g)
    print("g^a used public key : ", public_key)

    # Chiffrement de l'image
    img_data_str = "".join([chr(x) for x in img_data])
    encrypted_data, p = encrypt(img_data_str, q, public_key, g)
    print("Encrypted Image Size:", len(encrypted_data))

    # Déchiffrement de l'image
    decrypted_data = decrypt(encrypted_data, p, private_key, q)
    print("Decrypted Image Size:", len(decrypted_data))

    # Sauvegarde de l'image déchiffrée
    img_data_int = [ord(c) for c in decrypted_data]
    with Image.open(io.BytesIO(bytearray(img_data_int))) as img:
        img.save("decrypted_elgamal_image.png", format=img.format)
 
 
if __name__ == '__main__':
    main_text()
    main_image()