from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad
import base64
from PIL import Image
import io, os

def encrypt(key, iv, message):
    # Crée l'objet AES
    cipher = AES.new(pad(key.encode(), 32), AES.MODE_CBC, iv)
    # Chiffrement du message
    ciphertext = cipher.encrypt(pad(message, AES.block_size))
    # Affichage du message chiffré
    #print("Message chiffré :", base64.b64encode(ciphertext))
    return ciphertext
    
def decrypt(ciphertext, key, iv):
    # Crée l'objet AES
    cipher = AES.new(pad(key.encode(), 32), AES.MODE_CBC, iv)
    # Déchiffrement du message
    decrypted = unpad(cipher.decrypt(ciphertext), AES.block_size)
    return decrypted
    

def convert_img(filename):
    # Charge l'image à chiffrer
    with Image.open(filename) as img:
        # Conversion de l'image en données binaires
        img_bytes = io.BytesIO()
        img.save(img_bytes, format=img.format)
        img_bytes = img_bytes.getvalue()
    return img_bytes


if __name__ == "__main__":
    #Clé de chiffrement de 128 bits (16 octets)
    key = 'ma clé secrète 1'
    #Vecteur d'initialisation de 16 octets
    iv = os.urandom(16)
    #Message à chiffrer (doit être un multiple de 16 octets)
    message = 'Bonjour, monde!'.encode()
    msg_encrypted = encrypt(key, iv, message)
    # Affichage du message déchiffré
    print("Message déchiffré :", decrypt(msg_encrypted, key, iv).decode('utf-8'))
    
    
    image = convert_img('myimage.jpg')
    image_encrypted = encrypt(key, iv, image)
    with open('encrypted_image.jpg', 'wb') as f:
        f.write(image_encrypted)
    decrypted_image = decrypt(image_encrypted, key, iv)
    with open('decrypted_image.jpg', 'wb') as f:
        f.write(decrypted_image)